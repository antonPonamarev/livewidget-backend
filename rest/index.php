<?

    include_once 'config.php';    
    include_once '../classes/main.php';
    header('Content-Type: application/json');
    $json_input = file_get_contents("php://input");
    if(($data = json_decode($json_input, true)) != null)
        $_POST = $data;


    new DB;
    $action = ROUTE::getAction();
    $class = mb_strtoupper($action[0]);
    $method = $action[1]; 
    echo $class::$method();

?>