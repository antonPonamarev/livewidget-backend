<?

    class ADMIN
    {
        
        public static function checkToken()
        {
            $result = false;
            if (isset($_COOKIE['token']))
            {
                if ($_COOKIE['token']!="")
                {
                    $userId = DB::select("SELECT `client_id` FROM `clients` WHERE `token` = '{$_COOKIE['token']}'");
                    if (count($userId)==1)
                    {
                        $result = $userId[0]['client_id'];
                    }
                }

            }
            return $result;
        }

        public static function checkEmptyId()
        {
            if (!SYSTEM::checkPostArr($_POST['id']))
            {
                return SYSTEM::returnError('empty_id');
            }

            return false;
        }

        public static function getWidgetId()
        {
            return $_POST['id'];
        }
        
        public static function get_widget()
        {
            $check = ADMIN::checkEmptyId();
            if (!$check)
            {
                $id = ADMIN::getWidgetId();
                $widget = DB::select("SELECT * FROM `widgets` INNER JOIN `content` ON `widgets`.`video` = `content`.`content_id` WHERE `id` = ".$id."");
                if (count($widget)==1)
                {
                    return SYSTEM::returnSuccess($widget[0]);
                }

                return SYSTEM::returnError('not_found_id='.$id);
            }
       
            return $check;
      
        }

        public static function update_widget()
        {
            $check = ADMIN::checkEmptyId();
            if (!$check)
            {
                $id = ADMIN::getWidgetId();  
                if ($id == -1)
                {
                    return ADMIN::createWidget();
                }
                else 
                {
                    return ADMIN::update_exist_widget();
                }
            }
            else
            {
                return $check;
            }
        }

        
        private static function returnLastWidget($lastId=0)
        {
            if ($lastId == 0)
            {
                $lastId = DB::getLastInsertedId();
            }
            $insertedWidget = DB::select("SELECT * FROM `widgets` WHERE `id` = {$lastId}");
            if (count($insertedWidget == 1))
            {
                return SYSTEM::returnSuccess($insertedWidget[0]);
            }
            else
            {
                return SYSTEM::returnError('fail_to_create_widget');
            }
        }

        private static function createWidget()
        {
            unset($_POST['id']);
            $token = $_COOKIE["token"];
            $user = DB::select("SELECT * FROM `clients` WHERE `token` = $token");
            if(count($user) > 0){
                $_POST["client_id"] = $user[0]["client_id"];
            }
            
            //необязательно, даже скорее не желательно сбивать такие выкладки в одну строку 
            // не ясно назначение этой функции createTransferString
            // ` - необязательный символ 
            // ' - можно ставить везде, даже на int (вроде)

            $values = SYSTEM::createTransferString(array(array("elements"=>$_POST,"before"=>'"',"after"=>'"',"beforeAfterCond"=>"int")),"",",");
            $fields = SYSTEM::createTransferString(array(array("elements"=>array_keys($_POST),"before"=>'`',"after"=>'`')),"",",");
            DB::freeRequest("INSERT INTO `widgets` (".$fields.") VALUES (".$values.")");
            return ADMIN::returnLastWidget();
        }

        private static function update_exist_widget()
        {
            $widgetId = $_POST['id'];
            unset($_POST['id']);
            $updateStr = SYSTEM::createTransferString(
                array(
                    array(
                        "elements" => array_keys($_POST),
                        "before"=>"`",
                        "after"=>"`"
                    ),
                    array(
                        "elements" => $_POST,
                        "before"=>'"',
                        "after"=>'"',
                        "beforeAfterCond"=>"int"
                    )
                ),
                '=',
                ','
            );

       

            DB::freeRequest("UPDATE `widgets` SET {$updateStr} WHERE `id` = {$widgetId}");
            return ADMIN::returnLastWidget($widgetId);
        }

        public static function get_widgets()
        {
            if (!isset($_POST['client_id']))
            {
                $clientIdFromToken = ADMIN::checkToken();
                if (!$clientIdFromToken)
                {
                    return SYSTEM::returnError('empty_client_id_field');
                }
                else
                {
                    $_POST['client_id'] = $clientIdFromToken;
                }
            
            }

            if ($_POST['client_id'] == "")
            {
                $clientIdFromToken = ADMIN::checkToken();
                if (!$clientIdFromToken)
                {
                    return SYSTEM::returnError('empty_client_id');
                }
                else
                {
                    $_POST['client_id'] = $clientIdFromToken;
                }
            }
            $widgets = DB::select("SELECT * FROM `widgets` INNER JOIN `content` ON `widgets`.`video` = `content`.`content_id` WHERE `widgets`.`client_id` = {$_POST['client_id']}");
            if (count($widgets) == 0)
            {
                return SYSTEM::returnSuccess("client_width_id = {$_POST['client_id']} not_has_widgets");
            }
            else
            {
                return SYSTEM::returnSuccess($widgets);
            }
        }

        public static function previewWidget(){
            

            $widget = DB::select("SELECT * FROM `widgets` INNER JOIN `content` ON `widgets`.`video` = `content`.`content_id` WHERE `id` = {$_POST["id"]}");
            $client = DB::select("SELECT * FROM `clients` WHERE `token` = {$_COOKIE['token']}");

            return SYSTEM::returnSuccess(array(
                "client" => $client[0],
                "widget" => $widget[0],
                "token" => $_COOKIE['token']
            ));
        }


        public static function dashboard_short()
        {
            if (!isset($_POST['client_id']))
            {
                $clientIdFromToken = ADMIN::checkToken();
                if (!$clientIdFromToken)
                {
                    return SYSTEM::returnError('not recieved client_id POST parameter');
                }
                else
                {
                    $_POST['client_id'] = $clientIdFromToken;
                }
            }
            if ($_POST['client_id'] == "")
            {
                $clientIdFromToken = ADMIN::checkToken();
                if (!$clientIdFromToken)
                {
                    return SYSTEM::returnError('empty client_id');
                }
                else
                {
                    $_POST['client_id'] = $clientIdFromToken;
                }
            }
            $statistic['user_count'] = count(DB::select("SELECT `visitor_id` FROM `visits` INNER JOIN `widgets` ON `visits`.`widget_id` = `widgets`.`id` WHERE `widgets`.`client_id` = {$_POST['client_id']} GROUP BY `visitor_id`"));
            $allVisits = DB::select("SELECT `visit_type_id` FROM `visits` INNER JOIN `widgets` ON `visits`.`widget_id` = `widgets`.`id` WHERE `widgets`.`client_id` = {$_POST['client_id']}");
            foreach ($allVisits as $visit)
            {
                if ($visit['visit_type_id'] == 2)
                {
                    $statistic['clicks_count']++;
                }

                if ($visit['visit_type_id'] == 4)
                {
                    $statistic['lead_count']++;
                }
            }
            if ($statistic['lead_count'] == 0 || $statistic['clicks_count']==0)
            {
                $statistic['conversion'] = 0;
            }
            else
            {
                $statistic['conversion'] = $statistic['lead_count'] / $statistic['clicks_count'];
            }
            return SYSTEM::returnSuccess($statistic);
        }

        public static function get_widgets_stat()
        {
            if (!isset($_POST['client_id']))
            {
                $clientIdFromToken = ADMIN::checkToken();
                if (!$clientIdFromToken)
                {
                    return SYSTEM::returnError('not recieved client_id POST parameter');
                }
                else
                {
                    $_POST['client_id'] = $clientIdFromToken;
                }
            }
            if ($_POST['client_id'] == "")
            {
                $clientIdFromToken = ADMIN::checkToken();
                if (!$clientIdFromToken)
                {
                    return SYSTEM::returnError('empty client id');
                }
                else
                {
                    $_POST['client_id'] = $clientIdFromToken;
                }
            }
            $widgetsStat = DB::select("SELECT * FROM `requests` INNER JOIN `widgets` ON `requests`.`widget_id` = `widgets`.`id` WHERE `widgets`.`client_id` = {$_POST['client_id']}");
            if (count($widgetsStat) == 0)
            {
                return SYSTEM::returnError('not find request');
            }
            else
            {
                return SYSTEM::returnSuccess($widgetsStat);
            }
        }

        public static function upload_video()
        {

            $token = $_COOKIE["token"];
            $user = DB::select("SELECT * FROM `clients` WHERE `token` = $token");
            if(count($user) > 0){
                $client_id = $user[0]["client_id"];
            }
            else{
                SYSTEM::returnError('auth error');
            }
            if (!preg_match('/^video\/.*/',$_FILES['upload_file']['type']))
            {
                //return SYSTEM::returnError('not video format'.print_r($_FILES,true));
            }

            $path = $_SERVER['DOCUMENT_ROOT'].'/videos/'.$_FILES['upload_file']['name'];
            $url = 'https://'.$_SERVER['HTTP_HOST'].'/videos/'.$_FILES['upload_file']['name'];

            if (move_uploaded_file($_FILES['upload_file']['tmp_name'],$path))
            {


                DB::freeRequest("INSERT INTO `content` (`content_source`,`client_id`,`content_type`,`widget_id`) VALUES ('{$url}',{$client_id},'video',{$_POST['widget_id']})");
                $newVideoId = DB::getLastInsertedId();
                
                DB::freeRequest("UPDATE `widgets` SET `video` = {$newVideoId} WHERE `id` = {$_POST['widget_id']}");
                
                
                $newVideo = DB::select("SELECT * FROM `content` WHERE `content_id` = $newVideoId");
                return SYSTEM::returnSuccess($newVideo[0]);
            }
            else
            {
                SYSTEM::returnError('error to upload videoFile');
            }

        }

        public static function registration()
        {
            // if (!isset($_POST['login']))
            // {
            //     return SYSTEM::returnError('not get login');
            // }

            if (!isset($_POST['password']))
            {
                return SYSTEM::returnError('not get password');
            }

            if (!isset($_POST['domain']))
            {
                return SYSTEM::returnError('not get domain');
            }

            // if (($_POST['login'])=="")
            // {
            //     return SYSTEM::returnError('empty login');
            // }

            if (($_POST['password'])=="")
            {
                return SYSTEM::returnError('empty password');
            }

            if (($_POST['domain'])=="")
            {
                return SYSTEM::returnError('empty domain');
            }

            $mail = "";

            if (isset($_POST['email']))
            {
                if ($_POST['email']!="")
                {
                    $mail = $_POST['email'];
                }
            }
    
            $token = time();
            DB::freeRequest("INSERT INTO `clients` (`password`,`domain`,`email`,`token`) VALUES ('{$_POST['password']}','{$_POST['domain']}','{$mail}','{$token}')");
            $newClientId = DB::getLastInsertedId();
            $newClient = DB::select("SELECT * FROM `clients` WHERE `client_id` = {$newClientId}");
            setcookie("token",$token,time()+3600);
            return SYSTEM::returnSuccess($newClient[0]);
        }


        public static function init()
        {
          
            if(!isset($_COOKIE["token"])){
                return SYSTEM::returnError('1021');
            }
            else{
                $user = DB::select("SELECT * FROM `clients` WHERE `token` = '{$_COOKIE["token"]}'");


                if (count($user)!=1)
                {
                    return SYSTEM::returnError('1021');
                }
                else
                {
                    return SYSTEM::returnSuccess($user[0]);
                }
            }
        }

        public static function auth()
        {
            if (!isset($_POST['login']))
            {
                SYSTEM::returnError('not get login');
            }

            if (!isset($_POST['password']))
            {
                SYSTEM::returnError('not get password');
            }

            if (($_POST['login'])=="")
            {
                SYSTEM::returnError('empty login');
            }

            if (($_POST['password'])=="")
            {
                SYSTEM::returnError('empty password');
            }

            $user = DB::select("SELECT * FROM `clients` WHERE `email` = '{$_POST['email']}' AND `password` = '{$_POST['password']}'");
            $token = time();
            setcookie("token",$token,time()+3600);
            
            if (count($user)!=1)
            {
                return SYSTEM::returnError('not right combination login and password');
            }
            else
            {
                DB::freeRequest("UPDATE `clients` SET `token` = '{$token}' WHERE `client_id` = {$user[0]['client_id']}");
                $user = DB::select("SELECT * FROM `clients` WHERE `client_id` = {$user[0]['client_id']}");
                return SYSTEM::returnSuccess($user[0]);
            }
        }

    }

?>