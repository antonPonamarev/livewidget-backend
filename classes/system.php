<?

    class SYSTEM
    {
        public static function checkPostArr($arr)
        {
            if (is_array($arr))
            {
                foreach($arr as $element)
                {
                    if ($element == "")
                    {
                        return false;
                    }
                }

                return true;

            }
            else
            {
                if ($arr == "")
                {
                    return false;
                }


                return true;
            }
        }

        public static function returnError($errorCode)
        {
            return json_encode(
                array(
                    'status'=>'fail',
                    'error_code'=>$errorCode)
                );
        }

        public static function returnSuccess($successResult)
        {
            return json_encode(
                array(
                    'status'=>'success',
                    'result'=>$successResult
                )
            );
        }

        public static function createTransferString($elementsArr,$delimeter="",$arrDelimeter="")
        {
            $result = "";
            $arrLength = count($elementsArr);
            $count = 0;
            $length = count($elementsArr[0]['elements']);
            while ($count!=$length)
            {
                $arrCount = 0;
                while($arrCount!=$arrLength)
                {
                    $before = "";
                    $after = "";
                    $element = array_shift($elementsArr[$arrCount]['elements']);
                    if (isset($elementsArr[$arrCount]['before']))
                    {
                        $before = $elementsArr[$arrCount]['before'];
                    }
                    if (isset($elementsArr[$arrCount]['after']))
                    {
                        $after = $elementsArr[$arrCount]['after'];
                    }
                    if (isset($elementsArr[$arrCount]['beforeAfterCond']))
                    {
                        switch ($elementsArr[$arrCount]['beforeAfterCond']) {
                            case 'int':
                                if (intval($element))
                                {
                                    $after = "";
                                    $before = "";
                                }

                                break;
                            
                            default:

                                break;
                        }
                    }
                    $result.= $before.$element.$after;
                    if ($arrCount!=$arrLength-1)
                    {
                        $result.= $delimeter;
                    }
                    $arrCount++;
                }
                if ($count!=$length-1)
                {
                    $result.= $arrDelimeter;
                }
                $count++;
            }
     


            // foreach($elements['elements'] as $element)
            // {
            //     $before = $elementArr['before'];
            //     $after = $afterElement;
            //     switch ($beforeAfterCond) {
            //         case 'int':
            //             if (intval($element))
            //             {
            //                 $after = "";
            //                 $before = "";
            //             }
            //             break;
                    
            //         default:
            //             break;
            //     }
            //     if ($count!=count($elements)-1)
            //     {
            //         $result.= $before.$element.$after.$delimiter;
            //     }
            //     else
            //     {
                
            //         $result.= $before.$element.$after;
            //     }
            //     $count++;
            // }
            

            return $result;
        }

    }

?>