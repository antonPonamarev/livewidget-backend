<?

    class CLIENT
    {
        
        private static function setRandomCookie()
        {
            $randomCookie = md5(rand(0,1000).time());
            setcookie('user_cookie',$randomCookie,time()+6480000,'/');
            return $randomCookie;
        }

        
        private static function checkPOSTParameters()
        {
            if (!isset($_POST['widget_id']) || !isset($_POST['visit_type_id']) || !isset($_POST['url']))
            {
                return false;
            }

            if ($_POST['widget_id'] == "" || $_POST['visit_type_id'] == "" || $_POST['url'] == "")
            {
                return false;
            }

            return true;
        }

        public static function set_stat()
        {
            
            if (!CLIENT::checkPOSTParameters())
            {
                return SYSTEM::returnError('wron POST parameters');
            }
            
            if (!($_COOKIE['user_cookie']))
            {
                $visit = CLIENT::createNewVisitor(CLIENT::setRandomCookie(),$_POST['widget_id'],$_POST['visit_type_id'],$_POST['url']);
            }
            else
            {
                $checkCookie = DB::select("SELECT * FROM `visitors` WHERE `cookie` = '{$_COOKIE['user_cookie']}'");
                if (count($checkCookie) == 0)
                {
                    $visit =  CLIENT::createNewVisitor(CLIENT::setRandomCookie(),$_POST['widget_id'],$_POST['visit_type_id'],$_POST['url']);

                }
                else
                {
                    $visit = CLIENT::insertVisit($checkCookie[0]['visitor_id'],$_POST['widget_id'],$_POST['visit_type_id'],$_POST['url']);
                }
            }
     
            return SYSTEM::returnSuccess($visit);

        }

        private static function insertVisit($visitorId,$widgetId,$visitTypeId,$url)
        {
            $currentTime = time();
            DB::freeRequest("INSERT INTO `visits` (`visitor_id`,`widget_id`,`visit_type_id`,`visit_time`,`url`) VALUES ({$visitorId},{$widgetId},{$visitTypeId},'{$currentTime}','{$url}')");
            $insertId = DB::getLastInsertedId();
            $newVisit = DB::select("SELECT * FROM `visits` WHERE `visit_id` = {$insertId}");
            return $newVisit[0];
        }

        private static function createNewVisitor($cookie,$widgetId,$visitTypeId,$url)
        {
            DB::freeRequest("INSERT INTO `visitors` (`cookie`) VALUES ('{$cookie}')");
            $insertId = DB::getLastInsertedId();
            return CLIENT::insertVisit($insertId,$widgetId,$visitTypeId,$url);
        }

        public static function init()
        {
            if (!isset($_POST['client_id']))
            {
                return SYSTEM::returnError('not recieved client_id POST parameter');
            }
            if ($_POST['client_id'] == "")
            {
                return SYSTEM::returnError('empty client_id');
            }
            $widget = DB::select("SELECT * FROM `widgets` WHERE `client_id` = {$_POST['client_id']} LIMIT 1");
            if (count($widget) == 1)
            {
                return SYSTEM::returnSuccess($widget[0]);
            }
            else
            {
                return SYSTEM::returnError('not found widget for client with id = '.$_POST['client_id']);
            }
        }

        public static function send_form()
        {
            if ($_POST['phone']!="")
            {
                return SYSTEM::returnError('spam detected'); //защита от спама, как мы обычно делаем
            }
            DB::freeRequest("INSERT INTO `requests` (`request_mail`,`widget_id`,`request_phone`,`request_name`) VALUES ('{$_POST['mail']}',{$_POST['widget_id']},'{$_POST['really_p']}','{$_POST['name']}')");
            $newRequestId = DB::getLastInsertedId();
            $newRequest = DB::select("SELECT * FROM `requests` WHERE `request_id` = {$newRequestId}");
            return SYSTEM::returnSuccess($newRequest[0]);
        }
    }

?>