<?

    class DB
    {

        function __construct()
        {
            global $con;
            $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_NAME);
        }


        public static function select($sqlString)
        {
            global $con;
            $query = mysqli_query($con,$sqlString);
            while ($row = mysqli_fetch_assoc($query))
            {
                $result[] = $row;
            }
            return $result;
        }

        public static function freeRequest($sqlString)
        {
            global $con;
            mysqli_query($con,$sqlString);
        }

        public static function getLastInsertedId()
        {
            global $con;
            return mysqli_insert_id($con);
        }
    }

?>